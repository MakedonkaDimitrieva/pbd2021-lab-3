package si.uni_lj.fri.pbd.lab3

import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class DetailsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        val details = DetailsFragment()
        details.arguments = intent.extras
        supportFragmentManager.beginTransaction().add(android.R.id.content,
        details).commit()
        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            // If the screen is now in landscape mode, we can show the
            // details in-line so we don't need this activity.
            finish()
            return
        }
    }
}