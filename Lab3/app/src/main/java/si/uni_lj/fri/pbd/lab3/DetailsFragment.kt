package si.uni_lj.fri.pbd.lab3

import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ScrollView
import android.widget.TextView
import androidx.fragment.app.Fragment

class DetailsFragment : Fragment() {
    companion object {
        fun newInstance(index: Int): DetailsFragment {
            val f = DetailsFragment()
            // Supply index input as an argument.
            val args = Bundle()
            args.putInt("index", index)
            f.arguments = args
            return f
        }
    }

    fun getShownIndex(): Int {
        return arguments!!.getInt("index", 0);
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        if (container == null) {
            // Currently in a layout without a container, so no
            // reason to create our view
            return null
        }
        val scroller = ScrollView(activity)
        val text = TextView(activity)
        val padding = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 4f, activity!!.resources.displayMetrics
        ).toInt()
        text.setPadding(padding, padding, padding, padding)
        scroller.addView(text)
        text.text = resources.getStringArray(R.array.descriptions)[getShownIndex()]
        return scroller
    }
}