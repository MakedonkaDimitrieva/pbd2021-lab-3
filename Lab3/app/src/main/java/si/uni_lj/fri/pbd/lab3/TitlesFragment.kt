package si.uni_lj.fri.pbd.lab3

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.ListFragment
import androidx.fragment.app.FragmentTransaction

class TitlesFragment : ListFragment() {
    private var mDualPane: Boolean = false
    private var mCurCheckPosition: Int = 0

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // Populate list with our static array of titles.
        listAdapter = ArrayAdapter(
            activity!!,
            android.R.layout.simple_spinner_dropdown_item,
            resources.getStringArray(R.array.titles)
        )
        // Check to see if we have a frame in which to embed the details
        // fragment directly in the containing UI
        val detailsFrame: View? = activity?.findViewById(R.id.details)
        mDualPane = (detailsFrame != null) && (detailsFrame.visibility == View.VISIBLE)

        if(savedInstanceState != null) {
            mCurCheckPosition = savedInstanceState.getInt("currentChoice", 0);
        }

        if (mDualPane) {
            // In dual-pane mode, list view highlights selected item
            listView.choiceMode = ListView.CHOICE_MODE_SINGLE
            // Make sure our UI is in the correct state.
            showDetails(mCurCheckPosition)
        }
    }

    override fun onListItemClick(l: ListView, v: View, position: Int, id: Long) {
        super.onListItemClick(l, v, position, id)
        showDetails(position)
    }

    fun showDetails(index: Int) {
        mCurCheckPosition = index
        if (mDualPane) {
            // We can display everything in-place with fragments.
            // Have the list highlight this item and show the data.
            listView.setItemChecked(index, true)
            // Make new fragment to show this selection.
            val details: DetailsFragment = DetailsFragment.newInstance(index)
            // TODO: here you should replace the existing fragment in
            // "details" FrameLayout with the new fragment.
            // hint: use FragmentManager, don't forget to commit!

            val ft: FragmentTransaction = fragmentManager!!.beginTransaction()
            ft.replace(R.id.details, details)
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
            ft.commit()
        }
        else {
            // Otherwise we need to launch a new activity to display
            // the details fragment with selected text
            val intent = Intent()
            intent.setClass(activity!!, DetailsActivity::class.java)
            intent.putExtra("index", index)
            startActivity(intent)
        }
    }
}